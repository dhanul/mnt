/* Add your JS code Here */
(function(){
  let loanOption;
  var $scope;
  var debtIncomeRatio;
  var FICOScore;
  var incomeScore;
  var loanTenureScore;
  var CRDScore;
  var APRScore;
  var loanStatus;
  var reason;
  var loanIssueDate;
  var selectKey;
  var requestURL;
  var modelID;
  var empDocuments;
  var instanceID;
  var idProofFileName;
  var addressProofFileName;
  var empFileName;
  var idProofFilePath;
  var addressProofFilePath;
  var empFilePath;
  var grossAmount;
  var onFormLoad = function(){
	debugger;
	var _scope = this;
	$scope = this;
	var util = arguments[0];
	var resultMsgss = '';

	//$('.formio-component-Next1').hide();
	//$('.formio-component-Functioncolumns').hide();
	$('.formio-component-Continuebtn').hide();
	/*<div class="MainClass">\
  <label> 2. &nbsp;<span><b>SSN Number &nbsp;</b></span><span>- SSN Card</span>: </label>\
  </div>\*/
	try{
	  var url = location.href;
	  var param = atob(url.split("?")[1]);
	  loanOption = param.split(',')[0].split("=")[1];
	  var domOfCard='\
<div class = "container containerCss">\
<div class="MainClass">\
<label> 1. &nbsp;<span class="boldCss">Identity Proof &nbsp;</span><span>- Driving license/passport</span></label>\
</div>\
<div class="MainClass">\
<label> 2. &nbsp;<span class="boldCss">Address Proof &nbsp;</span><span>- Driving license/passport</span></label>\
</div>\
<div class="MainClass">\
<label> 3. &nbsp;<span>For salaried(only 1 document required) &nbsp;</span></label>\
</div>\
<div class="InnerClass">\
<label> a. &nbsp;<span class="boldCss">Last 2 years of W2 &nbsp;</span><span>( Annual Income statement)</span></label>\
</div>\
<div class="InnerClass">\
<label> b. &nbsp;<span class="boldCss">Pay Stub &nbsp;</span></label>\
</div>\
<div class="InnerClass">\
<label> c. &nbsp;<span class="boldCss">Personal Tax Form &nbsp;</span><span>( IRS Form 1040 )</span></label>\
</div>\
<div class="MainClass">\
<label> 5. &nbsp;<span>For self Employed (if applicable) &nbsp;</span></label>\
</div>\
<div class="InnerClass">\
<label> a. &nbsp;<span class="boldCss">Business Income statement &nbsp;</span><span></span></label>\
</div>\
</div>\
' ;
	  resultMsgss += domOfCard
	  $(".Proof").empty().append($(resultMsgss));
	}
	catch(e){ 

	} 
	$('.bs-wizard > div:nth-child(1)').addClass('Activestepper');
  }

  function getDateFormat(value){
	var date_input = new Date(value);
	var day = date_input.getDate();
	var month = date_input.getMonth() + 1;
	var year = date_input.getFullYear();
	var yyyy_MM_dd = day + "/" + month + "/" + year;
	return yyyy_MM_dd;
  }
  var callOnUploadFile = function(){

	debugger;
	$(".preloader-container").remove();
	NBT.ui.preloader.hide();
	var _scope = this;
	var util = arguments[0];

	_scope.submission.data["FileName"] = _scope.data["FileControl"][0]["FileControl.name"];
	_scope.submission.data["FileURL"] = _scope.data["FileControl"][0]["FileControl.url"];
	_scope.submission.data["FileSize"] = _scope.data["FileControl"][0]["FileControl.size"];


	if(selectKey == "IdentityProof"){
	  $("#form-group-AddressProof1").css("pointer-events","all");
	  $("#form-group-IdentityProof input").css("display","none");
	  $("#form-group-IdentityProof").addClass("checkboxCss");
	  _scope.submission.data["RequestURL"] = "Passport_DrivingLicenseUrl";
	  _scope.submission.data["ModelID"] = "Passport_DrivingLicenseModel";
	  $("#form-group-ThumbnailImg img").show();
	  $("#form-group-CloseImg img").show();
	  $("#form-group-Preview button").show();

	}else if(selectKey == "AddressProof1"){
	  $("#form-group-AddressProof1 input").css("display","none");
	  $("#form-group-AddressProof1").addClass("checkboxCss");
	  $("#form-group-ThumbnailImg1 img").show();
	  $("#form-group-CloseImg1 img").show();
	  $("#form-group-Preview1 button").show();
	  _scope.submission.data["RequestURL"] = "Passport_DrivingLicenseUrl";
	  _scope.submission.data["ModelID"] = "Passport_DrivingLicenseModel";
	  if ($("#form-group-EmployeeDocuments input").prop("checked")){
		$("#form-group-EmployeeDocuments").css("pointer-events","none");
	  }else{
		$("#form-group-EmployeeDocuments").css("pointer-events","all");
	  }

	}else if(selectKey == "EmployeeDocuments"){
	  _scope.submission.data["RequestURL"] = "Pay_studsUrl";
	  _scope.submission.data["ModelID"] = "PaystudModel";
	  if(empDocuments == "Salary"){
		$("#form-group-EmployeeDocuments .radio-inline:nth-child(1) input").css("display","none");
		$("#form-group-EmployeeDocuments .radio-inline:nth-child(1)").addClass("checkboxCss1");
		//$($("#form-group-EmployeeDocuments input:nth-child(1)")[0]).css("display","none");
		//$($("#form-group-EmployeeDocuments input:nth-child(1)")[0]).addClass("checkboxCss");
		$("#form-group-ThumbnailImg2 img").show();
		$("#form-group-Close3Img img").show();
		$("#form-group-Preview2 button").show();
		$("#form-group-EmployeeDocuments").css("pointer-events","none");
	  }else{
		$("#form-group-EmployeeDocuments .radio-inline:nth-child(2) input").css("display","none");
		$("#form-group-EmployeeDocuments .radio-inline:nth-child(2)").addClass("checkboxCss1");
		$("#form-group-ThumbnailImg3 img").show();
		$("#form-group-CloseImg3 img").show();
		$("#form-group-Preview3 button").show();
		$("#form-group-EmployeeDocuments").css("pointer-events","none");
	  }
	}


	if(selectKey == "" || selectKey == undefined){
	  NBT.util.modal.message("Message","Please select type of proof","Ok", function(res){
		if(res){
		  _scope.submission.data["FileControl"] = "";
		  _scope.$apply();
		}
	  });
	}else{
	  util.triggerLookupbyKey('GetPropertyKeyValueLookup', _scope, function (result) {
		$(".preloader-container").remove();
		NBT.ui.preloader.hide();
		requestURL = result[0].propertyValue;
		modelID = result[1].propertyValue;
	  });
	  var documentValue = [
		{
		  "name":_scope.submission.data["FileName"],
		  "size":_scope.submission.data["FileSize"],
		  "url":_scope.submission.data["FileURL"]
		}
	  ]
	  var workflowVariables = [
		{
		  "name":"document",
		  "value":JSON.stringify(documentValue)
		},{
		  "name":"requestUrl",
		  "value":requestURL
		},{
		  "name":"modelId",
		  "value":modelID
		}
	  ];

	  _scope.submission.data["ShowUploadedLabel"] = 1;

	  AmperAxp.WorkflowService.InitiateByName("ExtractData", "AzureIntegration", workflowVariables, false, function(response){
		$(".preloader-container").remove();
		NBT.ui.preloader.hide();
		console.log(response);
		_scope.submission.data["ShowUploadedLabel"] = 2;

		instanceID = response.data.instanceId;

		var result = response.data.output.analyzeResult.documentResults[0].fields;
		if(result.FirstName && result.FirstName.FirstName){
		  _scope.submission.data["FirstName"] = result.FirstName.FirstName;
		}
		if(result.LastName && result.LastName.LastName){
		  _scope.submission.data["LastName"] = result.LastName.LastName;
		}
		if(result.LastName && result.LastName.LastName){
		  _scope.submission.data["LastName"] = result.LastName.LastName;
		}
		if(result.Sex && result.Sex.Sex){
		  _scope.submission.data["Suffix"] = result.Sex.Sex == "F" ? "Mrs": "Mr";
		  _scope.$apply();
		}
		var dob;
		if(result.DateOfBirth && result.DateOfBirth.DateOfBirth){
		  var dateResult = result.DateOfBirth.DateOfBirth;
		  if(new Date(dateResult).getDate() > 12){
			dob = getDateFormat(result.DateOfBirth.DateOfBirth);
		  }else{
			dob = result.DateOfBirth.DateOfBirth;
		  }

		  _scope.submission.data["DOB"] = dob;
		  _scope.$apply();
		}
		if(result.Address && result.Address.Address){
		  _scope.submission.data["StreetAddress"] = result.Address.Address;
		}
		/* if(result.City && result.City.City){
		  _scope.submission.data["City"] = result.City.City;
		}*/
		if(result.PlaceOfLicense && result.PlaceOfLicense.PlaceOfLicense){
		  _scope.submission.data["State"] = result.PlaceOfLicense.PlaceOfLicense;
		}
		if(result.ZipCode && result.ZipCode.ZipCode){
		  _scope.submission.data["ZipCode"] = result.ZipCode.ZipCode;
		}
		if(result.Nationality && result.Nationality.Nationality){
		  _scope.submission.data["Citizenship"] = result.Nationality.Nationality;
		}
		if(result.GrossAmount && result.GrossAmount.GrossAmount){
		  grossAmount = result.GrossAmount.GrossAmount;
		}

		if(selectKey == "IdentityProof"){
		  idProofFileName = _scope.submission.data["FileName"];
		  idProofFilePath = "orgId_130522/instance_"+instanceID+"/"+_scope.submission.data["FileName"]+"";
		}
		if(selectKey == "AddressProof1"){
		  addressProofFileName = _scope.submission.data["FileName"];
		  addressProofFilePath = "orgId_130522/instance_"+instanceID+"/"+_scope.submission.data["FileName"]+"";
		}
		if(selectKey == "EmployeeDocuments"){
		  empFileName = _scope.submission.data["FileName"];
		  empFilePath = "orgId_130522/instance_"+instanceID+"/"+_scope.submission.data["FileName"]+"";
		}
		selectKey = "";
		setTimeout(function(){
		  _scope.submission.data["FileControl"] = "";
		  _scope.submission.data["ShowUploadedLabel"] = 0;
		  _scope.$apply();
		},1000);
	  });
	}
  }

  var confirmEmailAddress = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	if(_scope.submission.data["ConfirmEmailAddress1"] != _scope.submission.data["EmailAddress"]){
	  _scope.submission.data["ConfirmEmailAddress1"] = "";
	  _scope.formioForm.ConfirmEmailAddress1.$pristine = false;
	  setTimeout(function(){
		_scope.$apply();
	  },50);
	  // NBT.util.modal.message("Message","Confirm mail address should be same as mail address","Ok");
	}
  }
  var aprcalculation = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	const regex = /[^A-Za-z0-9]/g;
	var firstName = _scope.submission.data["FirstName"].toLowerCase();
	var totalIncome = _scope.submission.data["TotalIncome1"].replace(regex, "");
	var loanTenure = _scope.submission.data["LoanTenureSelect"];
	if(totalIncome >0 && totalIncome <= 999){
	  incomeScore = 0;
	}else if(totalIncome > 999 && totalIncome <= 4999){
	  incomeScore = 1.5;
	}else if(totalIncome > 4999 && totalIncome <= 9999){
	  incomeScore = 3;
	}else if(totalIncome > 9999 && totalIncome <= 14999){
	  incomeScore = 4.5;
	}else if(totalIncome > 14999 && totalIncome <= 19999){
	  incomeScore = 6;
	}else if(totalIncome > 19999 && totalIncome <= 24999){
	  incomeScore = 7.5;
	}else if(totalIncome > 24999 && totalIncome <= 99999){
	  incomeScore = 9;
	}else if(totalIncome > 99999){
	  incomeScore = 10;
	}
	if(loanTenure > 0 && loanTenure <= 5){
	  loanTenureScore = 10;
	}else if(loanTenure > 5 && loanTenure <= 15){
	  loanTenureScore = 8;
	}else if(loanTenure > 15 && loanTenure <= 25){
	  loanTenureScore = 6;
	}else if(loanTenure > 25 && loanTenure <= 40){
	  loanTenureScore = 4;
	}else if(loanTenure > 40 && loanTenure <= 60){
	  loanTenureScore = 3;
	}else if(loanTenure > 60 && loanTenure <= 80){
	  loanTenureScore = 1;
	}else if(loanTenure > 80){
	  loanTenureScore = 0;
	}
	if(firstName == "michael, m"){
	  debtIncomeRatio = 10;
	  FICOScore = 7.5;
	}else if(firstName == "rana masood"){
	  debtIncomeRatio = 8;
	  FICOScore = 6;
	}else if(firstName == "brandon bao"){
	  debtIncomeRatio = 6;
	  FICOScore = 4.5;
	}else if(firstName == "fn dominic"){
	  debtIncomeRatio = 4;
	  FICOScore = 3;
	}else if(firstName == "david"){
	  debtIncomeRatio = 2;
	  FICOScore = 1.5;
	}else if(firstName == "xyz"){
	  debtIncomeRatio = 0;
	  FICOScore = 0;
	}

	CRDScore = 2.5 * ( incomeScore + debtIncomeRatio + FICOScore + loanTenureScore ); 			

	_scope.submission.data["CRDScore"] = CRDScore;
	if(CRDScore > 0.01 && CRDScore <= 30){
	  APRScore = 6.5;
	}else if(CRDScore > 30.01 && CRDScore <= 31){
	  APRScore = 6.5;
	}else if(CRDScore > 31.01 && CRDScore <= 35){
	  APRScore = 7;
	}else if(CRDScore > 35.01 && CRDScore <= 40){
	  APRScore = 7.5;
	}else if(CRDScore > 40.01 && CRDScore <= 43){
	  APRScore = 8;
	}else if(CRDScore > 43.01 && CRDScore <= 46){
	  APRScore = 8.5;
	}else if(CRDScore > 46.01 && CRDScore <= 49){
	  APRScore = 9;
	}else if(CRDScore > 49.01 && CRDScore <= 52){
	  APRScore = 9.5;
	}else if(CRDScore > 52.01 && CRDScore <= 55){
	  APRScore = 10;
	}else if(CRDScore > 55.01 && CRDScore <= 58){
	  APRScore = 10.5;
	}else if(CRDScore > 58.01 && CRDScore <= 61){
	  APRScore = 11;
	}else if(CRDScore > 61.01 && CRDScore <= 64){
	  APRScore = 11.5;
	}else if(CRDScore > 64.01 && CRDScore <= 67){
	  APRScore = 12;
	}else if(CRDScore > 67.01 && CRDScore <= 70){
	  APRScore = 12.5;
	}else if(CRDScore > 70.01 && CRDScore <= 73){
	  APRScore = 13;
	}else if(CRDScore > 73.01 && CRDScore <= 76){
	  APRScore = 13.5;
	}else if(CRDScore > 76.01 && CRDScore <= 80){
	  APRScore = 13.74;
	}else if(CRDScore > 80.01 && CRDScore <= 100){
	  APRScore = 13.74;
	}
	if(CRDScore >0 && CRDScore <=30){
	  loanStatus = "Reject";
	}else if(CRDScore >30.01 && CRDScore <=80){
	  loanStatus = "Refer to Loan officer";
	  reason = "Medium CRD Score";
	}else if(CRDScore >80.01 && CRDScore <=100){
	  loanStatus = "Accept";
	}
	if(loanStatus == "Accept"){
	  loanIssueDate = new Date().toJSON().slice(0,10).split('-').reverse().join('/');
	}else{
	  loanIssueDate = "";
	}
	_scope.submission.data["LoanStatus"] = loanStatus;
	$(".rate").val(APRScore);
	$(".amount").val(_scope.submission.data["LoanAmount"]);
	$(".loanAmount").val(_scope.submission.data["LoanAmount"]);

  }
  var onClickContinue = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	$('.formio-component-TermsMainCol').hide();
	$('.formio-component-Continuebtn').hide();
	$('.formio-component-Next1').show();
	$('.formio-component-Functioncolumns').show();
	_scope.submission.data["LoanSelectionBack"] = 1;
	if(_scope.submission.data["LoanSelectionBack"] ==1 ){

	}
  }
  var onpageNavigation = function(){
	debugger;
	var resultMsgss = '';
	var _scope = this;
	var util = arguments[0];
	var $wizardScope = angular.element(document.getElementsByClassName('bs-wizard')[0]).scope();
	var util = arguments[0];
	var CurrentPage = $wizardScope.currentPage + 1;
	var NextPage = CurrentPage + 1;
	if($wizardScope.currentPage == 4){
	  setTimeout(function(){
		$('.formio-component-CongratColumns').hide();

		/*$('.formio-component-SubmitLast').hide();*/
	  },200);
	  $wizardScope.submission.data["FirstNameLP"] = _scope.submission.data["FirstName"];
	  $wizardScope.submission.data["MiddleNameLP"] = _scope.submission.data["MiddleName"];
	  $wizardScope.submission.data["LastNameLP"] = _scope.submission.data["LastName"];
	  $wizardScope.submission.data["AmounttobeTransferred"] = _scope.submission.data["LoanAmount"];
	  $wizardScope.submission.data["AmountFinanced"] = '$'+_scope.submission.data["LoanAmount"];

	  $wizardScope.submission.data["APR"] = APRScore +'%';
	  $wizardScope.submission.data["Upfrontfees"] = "$ 15000";
	  $wizardScope.submission.data["LoanTenurePayment"] = _scope.submission.data["LoanTenureSelect"];
	  $wizardScope.submission.data["CRDScore"] = loanStatus;
	  var loanamount=_scope.submission.data["LoanAmount"];
	  var Interestrate=APRScore;
	  var Installments=_scope.submission.data["LoanTenureSelect"];
	  /*EMI = [P x R x (1+R)^N]/[(1+R)^N-1]*/
	  var InterestratePermonth=APRScore/(12*100);/*R*/
	  var IR=1+InterestratePermonth;
	  var a = Math.pow(IR, Installments);
	  var upper=loanamount*InterestratePermonth*a;
	  var lower=a-1;
	  var EMI=upper/lower;
	  var totalofpayment=EMI*Installments;
	  var totalinterest=totalofpayment-loanamount;
	  var FinalEMI=Math.round(EMI);
	  var Finaltotalofpayment=Math.round(totalofpayment);
	  var Finaltotalinterest=Math.round(totalinterest)
	  var allpaymentsandfees=Finaltotalofpayment+1500;
	  $wizardScope.submission.data["PaymentEveryMonth"]='$'+''+ FinalEMI;
	  $wizardScope.submission.data["TotalofPayments"]='$'+''+Finaltotalofpayment;
	  $wizardScope.submission.data["TotalInterest"]='$'+''+Finaltotalinterest;
	  $wizardScope.submission.data["Allpaymentandfees"]='$'+''+allpaymentsandfees;
	  
	  

	}
	else if($wizardScope.currentPage == 0){
	  /* setTimeout(function(){
		  $('.formio-component-TermsMainCol').hide();
		  $('.formio-component-Continuebtn').hide();
		  $('.formio-component-Next1').show();
		  $('.formio-component-Functioncolumns').show();
		},200);*/
	}
	else if($wizardScope.currentPage == 1){
	  setTimeout(function(){
		util.triggerLookupbyKey('GetCountriesLookup', _scope, function (result) {
		});
		if ($("#form-group-IdentityProof input").prop("checked")) {
		  $("#form-group-ThumbnailImg img").show();
		  $("#form-group-CloseImg img").show();
		  $("#form-group-Preview button").show();
		  $("#form-group-AddressProof1").css("pointer-events","all");
		  $("#form-group-IdentityProof input").css("display","none");
		  $("#form-group-IdentityProof").addClass("checkboxCss");
		}else{
		  $("#form-group-ThumbnailImg img").hide();
		  $("#form-group-CloseImg img").hide();
		  $("#form-group-Preview button").hide();
		  $("#form-group-AddressProof1").css("pointer-events","none");
		  $("#form-group-IdentityProof input").css("display","block");
		  $("#form-group-IdentityProof").removeClass("checkboxCss");
		}

		if ($("#form-group-AddressProof1 input").prop("checked")) {
		  $("#form-group-ThumbnailImg1 img").show();
		  $("#form-group-CloseImg1 img").show();
		  $("#form-group-Preview1 button").show();
		  $("#form-group-EmployeeDocuments").css("pointer-events","all");
		  $("#form-group-AddressProof1 input").css("display","none");
		  $("#form-group-AddressProof1").addClass("checkboxCss");
		}else{
		  $("#form-group-ThumbnailImg1 img").hide();
		  $("#form-group-CloseImg1 img").hide();
		  $("#form-group-Preview1 button").hide();
		  $("#form-group-EmployeeDocuments").css("pointer-events","none");
		  $("#form-group-AddressProof1 input").css("display","block");
		  $("#form-group-AddressProof1").removeClass("checkboxCss");
		}
		if ($("#form-group-EmployeeDocuments .radio-inline:nth-child(1) input").prop("checked")) {
		  $("#form-group-ThumbnailImg2 img").show();
		  $("#form-group-Close3Img img").show();
		  $("#form-group-Preview2 button").show();
		  $("#form-group-EmployeeDocuments .radio-inline:nth-child(1) input").css("display","none");
		  $("#form-group-EmployeeDocuments .radio-inline:nth-child(1)").addClass("checkboxCss1");
		  $("#form-group-EmployeeDocuments").css("pointer-events","none");
		}else{
		  $("#form-group-ThumbnailImg2 img").hide();
		  $("#form-group-Close3Img img").hide();
		  $("#form-group-Preview2 button").hide();
		  $("#form-group-EmployeeDocuments .radio-inline:nth-child(1) input").css("display","block");
		  $("#form-group-EmployeeDocuments .radio-inline:nth-child(1)").removeClass("checkboxCss1");
		}

		if ($("#form-group-EmployeeDocuments .radio-inline:nth-child(2) input").prop("checked")) {
		  $("#form-group-ThumbnailImg3 img").show();
		  $("#form-group-CloseImg3 img").show();
		  $("#form-group-Preview3 button").show();
		  $("#form-group-EmployeeDocuments .radio-inline:nth-child(2) input").css("display","none");
		  $("#form-group-EmployeeDocuments .radio-inline:nth-child(2)").addClass("checkboxCss1");
		  $("#form-group-EmployeeDocuments").css("pointer-events","none");
		}else{
		  $("#form-group-ThumbnailImg3 img").hide();
		  $("#form-group-CloseImg3 img").hide();
		  $("#form-group-Preview3 button").hide();
		  $("#form-group-EmployeeDocuments .radio-inline:nth-child(1) input").css("display","block");
		  $("#form-group-EmployeeDocuments .radio-inline:nth-child(1)").removeClass("checkboxCss1");
		}

		/*$("#form-group-StepperFormcolumns img").hide();
		  $("#form-group-LabelColumns img").hide();
		  $("#form-group-StepperFormcolumns button").hide();
		  $("#form-group-LabelColumns button").hide();*/
	  },100);
	}
	else if($wizardScope.currentPage == 2){

	  try{
		var domOfCard='\
<div class = "LoanAgreement">\
<div class="firstDiv">\
<h3>Loan Agreement</h2>\
<p>THIS AGREEMENT, made entered into this &nbsp;<input type="text" class="day"> day of &nbsp;<input type="text" class="month"> 20&nbsp;<input type="text" class="year"> , by and between (business name and address), New York &nbsp; <input type="text" class="no">hereinafter sometimes referred to as "Company", and ABC Bank E-community Fund, hereinafter someimes referred to as "provider" for Network New York Funds.</p>\
</div>\
<div class="secondDiv">\
<h3>Witnesseth:</h3>\
<p>WHEREAS, the Company/Individual applied for a loan of <input type="text" class="loanAmount"> to be paid out of proceeds received by Provider from Network New York E-Community Fund and</p>\
<p>WHEREAS, the ABC Program Administrator is authorised to make this loan under application New York Law.</p>\
<p>Now THEREFORE, in consideration of the mutual Promises, convenants and agreements, the parties agree as follows: </p>\
<p class="loanLeft">1.This loan agreement is subject to:</p>\
<div class="subDiLoan">\
<p>a. The accuracy of representations made by the Company to <input type="text" class="company"> in the application and documentation presented by the Company/ Individual.</p>\
<p>b. The Provider\'s determination, in it\'s sole discretion, that there has been to unremedied adverse change in the financial or any other condition of the Company/ Individual initial application.</p>\
<p>c. An Areement executed by the Company/ Inidividual providing that in the event the Company/ Individual refiances it\'s dept to the Provider, relocates the another area outside of the State of New York, or sells 30% or more of it\'s assets, this loan shall be accelerated and immediately due and payable. Further , the loan may be immediately due and payable. Further, the loan may be immediately due and payable in the event there is a change ownership or control of the business without prior consent of the ABC Bank.</p>\
<p>d. Execution of a note in the principle amount of <input type="text" class="amount"> in the form and manner designated by the Provider, including but not limited to the following provisions.</p>\
<p class="loanLeft1">i. Commencing on the date of executionof the loan agreement between the Provider and the Company/ Individual, any outstanding note balance shall draw interest at the rate of <input type="text" class="rate"> per annum, with monthly principle and interest payments.</p>\
</div>\
</div>\
</div>\
' ;
		resultMsgss += domOfCard
		setTimeout(function(){
		  $(".Proof").empty().append($(resultMsgss));
		  $(".day").val(new Date().toJSON().slice(0,10).split('-').reverse().join('/').split(" ")[0].split("/")[0]);
		  $(".year").val(new Date().toJSON().slice(0,10).split('-').reverse().join('/').split(" ")[0].split("/")[2].slice(2,4));
		  $(".month").val(new Date().toLocaleString('default', { month: 'long' }));
		  $(".no").val("3740");
		  $(".company").val("ABC");
		  $(".rate").val(APRScore);
		  $(".amount").val(_scope.submission.data["LoanAmount"]);
		  $(".loanAmount").val(_scope.submission.data["LoanAmount"]);
		  _scope.submission.data["TotalIncome1"] = grossAmount;
		},200);
	  }
	  catch(e){ 

	  }
	  //$wizardScope.$apply();
	}
	else  if($wizardScope.currentPage == 3){


	  try{
		var domOfCard='\
<div class = "TermsCond">\
<h3>Terms and Conditions</h3>\
<p>The applicant should provide all the documents as per ABC Personal Loan policy and should fulfill the criteria mentioned earlier. All applicants will be scrutinized as per ABC Personal Loan policy and eligible/ verified applications will be processed further.</p>\
<p>Processing fee for ABC Personal Loan is as defined in the Schedule of Bank Charges( depending on the category of customer that you fall under) and will be charged and approval only.</p>\
<p>5% of the outstanding loan amount plus FED will be charged in case the customer wants to opt for early repayment. Any failure or neglect on part of the cutomer in making the payment of the purchase price on the dates specified shall constitue a default in payment by the customer. In such event, ABC shall be entitled to terminate the facility and recall the entire amount of finance outstanding then. In case of death, the remaning amount will be covered by the insurance company. </p>\
<p>The Bank has the right to cancel the facility under this agreement at any time. However, prior to cancellation, a notice to this effect shall be sent to the customer intimating the reason and demanding payment of loan within the period as mentioned in the said notice.</p>\
</div>\
<div class="declaration">\
<h3>Declaration:</h3>\
<ul>\
<li>I declare and confirm that all information stated herein and in other documents provided the bank by me or at my request is true and accurate in all respects. My loan liabilities, to all the sources, institutions and persons, have been fully and accurately reported in this application form and there are no liabilites on me other than the one stand herein. Further ,I declare I have not been defaulter of any financial institution in the past, neither in my personal capacity,not as proprietro/ partner of any business concern.</li>\
<li>I hereby undertake  that I will not obtain any form my employer against my end of service benefits until and unless the entire amount payable by me to ABC has been liquidated and clearance certificate is issued by ABC in this respect. ABC is also authorized to settle any outstanding loan amount from the funds transferred into my account as end of services benefits by my employee</li>\
<li>I hereby authorize ABC, and and any relevand thrid parties to exchange information for the purpose of processing my application including conduct of my account</li>\
<li>I hereby authorize ABC, to deduct its Processing Fee from the loan amount sanctioned to me as per the schedule of charges and also understand the all fees regarding the personal loan will be applicable to me and in my responsibility  to keep up-to-date with the latest Schedule of Charges of the Bank Which is available to me via the branches and the website. I hereby authorize ABC, to take out life insurance cover for and in my name. Such insurance cover will be part of the Master Policy taken by ABC, from an insurance Company for the borrower of ABC and I hereby authorize ABC to obtain  from any physician, hosipital, clinic, insurance company or other organization institution or person that has any record or knowledge of me or my health to give to ABC/ Insurance Company all information about me with reference to my health and medical history and any hospitalization, advice, treatment or disease or ailments</li>\
<li>I shall not have any resource to ABC, in any dispute whatever arising between me or my legal heirs with the company and i will not implied ABC in any proceedings which I may adopt to which I maybe party in any disputes or differences with the insurance Company and I will at all times keep ABC and it\'s branches indemnified from and against any claims against the insurance Company. The aforesaid complaints will bind me, my legal heirs and/ or representatives. I have read the package insurance plan taken by </li>\
</ul>\
</div>\
' ;
		resultMsgss += domOfCard
		setTimeout(function(){
		  $(".proof").empty().append($(resultMsgss));
		},200);
	  }
	  catch(e){ 

	  }
	  $wizardScope.$apply();
	}
  }

  var showConfirmationPage = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	$('.formio-component-LastPageColumns').hide();
	$('.formio-component-SubmitLast').hide();
	$('.formio-component-BackBtnLast').hide();
	$('.formio-component-CongratColumns').show();
	var requestBody = {
	  "CustomerInfo":{
		"ProfessionID":0,
		"GeographyID":0,
		"LoanTypeID":0,
		"FirstName":_scope.submission.data["FirstName"],
		"MiddleName":_scope.submission.data["MiddleName"],
		"LastName":_scope.submission.data["LastName"],
		"Suffix":_scope.submission.data["Suffix"],
		"DOB":_scope.submission.data["DOB"],
		"Income":_scope.submission.data["TotalIncome1"],
		"PoliticalExposure":0,
		"FICOScore":FICOScore,
		"IncomeScore":incomeScore,
		"Debt/IncomeScore":"",
		"TenureScore":loanTenureScore,
		"CRDScore":CRDScore,
		"APRScore":APRScore,
		"DebtIncomeRatio":"",
		"Networth":"",
		"LoanOption":loanOption,
		"LoanValue":_scope.submission.data["LoanAmount"],
		"LoanIssueDate":loanIssueDate,
		"CurrentDebtObligation":"",
		"Address":_scope.submission.data["StreetAddress"],
		"SuitNumber":_scope.submission.data["APT"],
		"City":_scope.submission.data["City"],
		"State":_scope.submission.data["State"],
		"ZipCode":_scope.submission.data["Zipcode"],
		"Citizenship":_scope.submission.data["Citizenship"],
		"OtherCitizenships":_scope.submission.data["MultiselectCountry"].toString(),
		"EmailID":_scope.submission.data["EmailAddress"],
		"SocialSecurityNumber":parseInt(_scope.submission.data["SocialSecurityNumber"]),
		"PhoneNumber":_scope.submission.data["LoanAmount"],
		"LengthOfTimeAtResidenceYears":_scope.submission.data["Dateval"],
		"LengthOfTimeAtResidenceMonths":_scope.submission.data["Monthval"],
		"HouseStatus":_scope.submission.data["RentStatus"],
		"CheckingAccountInFinancialInstitution":_scope.submission.data["StepperadioField4"],
		"SavingsAccountInFinancialInstitution":_scope.submission.data["StepperadioField5"],
		"AmperUserID":_scope.submission.data["AmperUserID"]
	  },
	  "LoanDetails":{
		"LoanAmount":_scope.submission.data["LoanAmount"],
		"LoanTenure":parseInt(_scope.submission.data["LoanTenureSelect"]),
		"TotalAnnualBaseIncome":_scope.submission.data["TotalIncome1"],
		"IsCoBorrower":_scope.submission.data["StepperadioField6"],
		"LoanFor":_scope.submission.data["StepperadioField7"],
		"LoanForOther":_scope.submission.data["OtherBorrower"],
		"CreditType":_scope.submission.data["StepperadioField1"],
		"IsCurrentBankAccountExist":_scope.submission.data["StepperadioField2"],
		"AccountType":_scope.submission.data["StepperadioField3"],
		"AccountNumber":parseInt(_scope.submission.data["AccountNumber"]),
		"BankNumber":parseInt(_scope.submission.data["BankAccountNumber"]),
		"RoutingNumber":parseInt(_scope.submission.data["RoutingNumber"]),
		"Reason":reason,
		"LoanStatus":loanStatus
	  },
	  "CustomDocuments":{
		"IdProofFileName":idProofFileName,
		"IdProofFilePath":idProofFilePath,
		"AddressProofFileName":addressProofFileName,
		"AddressProofFilePath":addressProofFilePath,
		"EmployeeFileName":empFileName,
		"EmployeeFilePath":empFilePath,
	  }
	}
	requestBody = JSON.stringify(requestBody);
	var workflowVariables = [
	  {
		"name":"RequestBody",
		"value":requestBody
	  }
	];

	NBT.ui.preloader.show();
	AmperAxp.WorkflowService.InitiateByName("UpsertCustomerLoan", "CuatomerLoan", workflowVariables, true, function(response){
	  console.log(response);
	  NBT.ui.preloader.hide();
	});
  }

  var onClickNext = function(){
	debugger;
	var _scope = this;
	var $wizardScope = angular.element(document.getElementsByClassName('bs-wizard')[0]).scope();
	var util = arguments[0];
	var CurrentPage = $wizardScope.currentPage + 1;
	var NextPage = CurrentPage + 1;
	$('.bs-wizard > div:nth-child('+CurrentPage+') ul.nav').addClass('activeclick');
	$('.bs-wizard > div:nth-child('+CurrentPage+')').removeClass('Activestepper');
	$('.bs-wizard > div:nth-child('+NextPage+')').addClass('Activestepper');	
	$('.bs-wizard > div:nth-child('+CurrentPage+')').addClass('stepperAfter');
	//$wizardScope.goto(1);
	var resultMsgss = '';
	if($wizardScope.currentPage == 0){
	  $wizardScope.goto(1);
	}else if($wizardScope.currentPage == 1){
	  $wizardScope.goto(2);
	}else if($wizardScope.currentPage == 2){
	  $wizardScope.goto(3);
	}else if($wizardScope.currentPage == 3){
	  $wizardScope.goto(4);
	}else if($wizardScope.currentPage == 4){
	  //$scope.goto(4);
	}else{}

	NBT.ui.preloader.show();
	setTimeout(function(){
	  if($wizardScope.currentPage == 1){
		try{
		  var domOfCard='\
<div class = "container containerCss">\
<div class="MainClass">\
<label> 1. &nbsp;<span><b>Identity Proof &nbsp;</b></span><span>- Driving license/passport</span></label>\
</div>\
<div class="MainClass">\
<label> 2. &nbsp;<span><b>SSN Number &nbsp;</b></span><span>- SSN Card</span>: </label>\
</div>\
<div class="MainClass">\
<label> 3. &nbsp;<span><b>Address Proof &nbsp;</b></span><span>- Driving license/passport</span></label>\
</div>\
<div class="MainClass">\
<label> 4. &nbsp;<span>For an employee &nbsp;</span></label>\
</div>\
<div class="InnerClass">\
<label> a. &nbsp;<span><b>Last 2 years of W2 &nbsp;</b></span><span>( Annual Income statement)</span></label>\
</div>\
<div class="InnerClass">\
<label> b. &nbsp;<span><b>Pay Stub &nbsp;</b></span></label>\
</div>\
<div class="InnerClass">\
<label> c. &nbsp;<span><b>Personal Tax Form &nbsp;</b></span><span>( IRS Form 1040 )</span></label>\
</div>\
<div class="MainClass">\
<label> 5. &nbsp;<span>For a Business man &nbsp;</span></label>\
</div>\
<div class="InnerClass">\
<label> a. &nbsp;<span><b>Business Income statement &nbsp;</b></span><span>(if applicable)</span></label>\
</div>\
</div>\
' ;
		  resultMsgss += domOfCard
		  $(".Proof").empty().append($(resultMsgss));
		}
		catch(e){ 

		}
		$wizardScope.$apply();
	  }

	  NBT.ui.preloader.hide();

	},100);
  }

  var onBackClick = function(){
	var _scope = this;
	var util = arguments[0];
	if(_scope.submission.data["LoanSelectionBack"] == 1){
	  $('.formio-component-TermsMainCol').show();
	  $('.formio-component-Continuebtn').show();
	  $('.formio-component-Next1').hide();
	  $('.formio-component-Functioncolumns').hide();
	  _scope.submission.data["StepperadioField1"] = "";
	  _scope.submission.data["StepperadioField2"] = "";
	  _scope.submission.data["LoanSelectionBack"] == 0;
	  //  _scope.$apply();
	}else{
	  window.location = location.origin + "/web/wns_mnt/apply-loan";	 
	}
  }

  var onClickBackPage = function(){
	debugger;
	var _scope = this;
	var $wizardScope = angular.element(document.getElementsByClassName('bs-wizard')[0]).scope();
	var util = arguments[0];
	var CurrentPage = $wizardScope.currentPage + 1;
	//$('.bs-wizard > div:nth-child('+CurrentPage+') ul.nav').addClass('activeclick');	
	//$wizardScope.goto(1);
	var resultMsgss = '';
	if($wizardScope.currentPage == 0){
	  //$wizardScope.goto(1);
	}else if($wizardScope.currentPage == 1){
	  $wizardScope.goto(0);
	}else if($wizardScope.currentPage == 2){
	  $wizardScope.goto(1);
	}else if($wizardScope.currentPage == 3){
	  $wizardScope.goto(2);
	}else if($wizardScope.currentPage == 4){
	  $wizardScope.goto(3);
	}else{}

	NBT.ui.preloader.hide();
  }

  $(document).on('click','.proofCss input',function(){
	selectKey = "IdentityProof";
  });

  $(document).on('click','.proof1Css input',function(){
	selectKey = "AddressProof1";
  });

  $(document).on('click','#form-group-EmployeeDocuments .radio-inline:nth-child(1)',function(){
	debugger;
	selectKey = "EmployeeDocuments";
	empDocuments = "Salary";
  });

  $(document).on('click','#form-group-EmployeeDocuments .radio-inline:nth-child(2)',function(){
	debugger;
	selectKey = "EmployeeDocuments";
	empDocuments = "Business";
  });

  $(document).on('click','.formio-component-CloseImg img',function(){
	debugger;
	$("form-group-DataCol input").val("")
	$("#form-group-ThumbnailImg img").hide();
	$("#form-group-CloseImg img").hide();
	$("#form-group-Preview button").hide();
	$("#form-group-IdentityProof").css("pointer-events","all");
	$("#form-group-IdentityProof input").css("display","block");
	$("#form-group-IdentityProof").removeClass("checkboxCss");
	$scope.submission.data["IdentityProof"] = "";
	selectKey = "";
	$scope.$apply();
	setTimeout(function(){
	  $("form-group-DataCol input").val("");
	},100);
  });


  $(document).on('click','.formio-component-CloseImg1 img',function(){
	debugger;

	$("#form-group-ThumbnailImg1 img").hide();
	$("#form-group-CloseImg1 img").hide();
	$("#form-group-Preview1 button").hide();
	$("#form-group-AddressProof1").css("pointer-events","all");
	$("#form-group-AddressProof1 input").css("display","block");
	$("#form-group-AddressProof1").removeClass("checkboxCss");
	$scope.submission.data["AddressProof1"] = "";
	$scope.$apply();
	selectKey = "";
	setTimeout(function(){
	  $("form-group-DataCol input").val("");
	},100);
  });


  $(document).on('click','.formio-component-Close3Img img',function(){
	debugger;
	$("#form-group-ThumbnailImg2 img").hide();
	$("#form-group-Close3Img img").hide();
	$("#form-group-Preview2 button").hide();
	$("#form-group-EmployeeDocuments .radio-inline:nth-child(1) input").css("display","block");
	$("#form-group-EmployeeDocuments .radio-inline:nth-child(1)").removeClass("checkboxCss1");
	$("#form-group-EmployeeDocuments").css("pointer-events","all");
	$scope.submission.data["EmployeeDocuments"] = "";
	$scope.$apply();
	selectKey = "";
  });

  $(document).on('click','.formio-component-CloseImg3 img',function(){
	debugger;
	$("#form-group-ThumbnailImg3 img").hide();
	$("#form-group-CloseImg3 img").hide();
	$("#form-group-Preview3 button").hide();
	selectKey = "";
	$("#form-group-EmployeeDocuments").css("pointer-events","all");
	$("#form-group-EmployeeDocuments .radio-inline:nth-child(1) input").css("display","block");
	$("#form-group-EmployeeDocuments .radio-inline:nth-child(1)").removeClass("checkboxCss1");
	$scope.submission.data["EmployeeDocuments"] = "";
	$scope.$apply();
  });

  $(document).on('click','.formio-component-Preview button',function(){
	debugger;
	window.open(location.origin+"/image-services/doc/"+idProofFilePath,'_blank');
  });
  $(document).on('click','.formio-component-Preview1 button',function(){
	debugger;
	window.open(location.origin+"/image-services/doc/"+addressProofFilePath,'_blank');
  });
  $(document).on('click','.formio-component-Preview2 button',function(){
	debugger;
	window.open(location.origin+"/image-services/doc/"+empFilePath,'_blank');
  });
  $(document).on('click','.formio-component-Preview3 button',function(){
	debugger;
	window.open(location.origin+"/image-services/doc/"+empFilePath,'_blank');
  });

  $(document).on("mouseenter","#form-group-coborrowerImg img", function(){
	debugger;
	$("#form-group-coborrowerImg img").parent().append('<div id="tooltipCss" style="display:block"><ul><li>ABC co-borrower loans on average are 25% larger</li><li>ABC co-borrower loans on average receive a 0.50% lower fixed APR</li></ul></div>');
	//$("#tooltipCss").css("display","block");
	/*$("#form-group-coborrowerImg img").attr("data-html",true);
	  $("#form-group-coborrowerImg img").attr("template",'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>');
	  $("#form-group-coborrowerImg img").tooltip();*/
  });

  $(document).on("mouseleave","#form-group-coborrowerImg img", function(){
	debugger;
	$("#tooltipCss").remove();
  });
  return { 
	onClickNext:onClickNext,
	onFormLoad:onFormLoad,
	onClickContinue:onClickContinue,
	onBackClick:onBackClick,
	onClickBackPage:onClickBackPage,
	onpageNavigation:onpageNavigation,
	callOnUploadFile:callOnUploadFile,
	aprcalculation:aprcalculation,
	showConfirmationPage:showConfirmationPage,
	confirmEmailAddress:confirmEmailAddress
  }

}());