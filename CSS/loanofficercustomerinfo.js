/* Add your JS code Here */
(function(){

  var loadSciptFiles = ['https://cdnjs.cloudflare.com/ajax/libs/echarts/4.1.0/echarts-en.common.min.js','https://cdn.anychart.com/js/latest/anychart.min.js','https://cdnjs.cloudflare.com/ajax/libs/echarts/5.1.0/echarts.min.js','https://cdnjs.cloudflare.com/ajax/libs/echarts/5.1.0/echarts.common.min.js','https://cdnjs.cloudflare.com/ajax/libs/echarts/5.1.0/echarts.esm.min.js','https://cdnjs.cloudflare.com/ajax/libs/echarts/5.1.0/echarts.simple.min.js','https://cdnjs.cloudflare.com/ajax/libs/echarts/4.6.0/echarts-en.min.js','https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js','https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.4.0/dist/chartjs-plugin-datalabels.min.js']
  for(var i=0;i<loadSciptFiles.length;i++){
	var xhrObj = new XMLHttpRequest();
	// open and send a synchronous request
	xhrObj.open('GET', loadSciptFiles[i], false);
	xhrObj.send('');
	// add the returned content to a newly created script tag
	var se = document.createElement('script');
	se.type = "text/javascript";
	se.text = xhrObj.responseText;
	document.getElementsByTagName('head')[0].appendChild(se);
  } 
  var deltakBarEchart = function(){

	debugger;
	var _scope = this;
	var util = arguments[0];
	NBT.ui.preloader.show();
	var ConsignedData = [];
	var firstDays = [];
	var secondDays = [];
	var thirdDays = [];
	var calcNorm=[];
	var RevNorm=[];
	var xaxisMonth = [];
	var hospitalNameArr = [];
	var hospitaltooltip = 0;
	var url = location.href;
	var param = atob(url.split("?")[1]);
	var loanOption = param.split(',')[0].split("=")[1];
	_scope.submission.data["CustomerID"]=loanOption;
	util.triggerLookupbyKey('GetCustomerLoanDetailslookup', _scope, function (result) {
	  debugger;if(result.length>0){
		if(result[0].AccountType == 'Savings'){
		  $('.formio-component-SavingsAccount input').prop('checked', true);
		}else if(result[0].AccountType == 'Checking'){
		  $('.formio-component-CurrentAccount input').prop('checked', true); 
		}else if(result[0].AccountType == 'Deposits'){
		  $('.formio-component-DepositAccountCheckBox input').prop('checked', true);
		}
	  }
	});
	util.triggerLookupbyKey('GetLoanDetailsforApprovalLookup', _scope, function (res) {
	  debugger;
	});
	//var chartDom = document.getElementById('main');
	jQuery(".barChartContainer1").append('<div id="mainStack" style="height:350px;margin-left:5px"></div>');
	var myChart = echarts.init(document.getElementById('mainStack'));
	//var myChart = echarts.init(chartDom);
	var option;
	option = {
	  series: [
		{
		  type: 'gauge',
		  startAngle: 180,
		  endAngle: 0,
		  axisLine: {

			lineStyle: {
			  width: -10,
			  color: [
				[0.3, '#EF5350'],
				[0.7, '#FFB11F'],
				[1, '#66BB6A']
			  ]
			}
		  },
		  pointer: {
			icon: 'path://M12.8,0.7l12,40.1H0.7L12.8,0.7z',
			width: 3,
			itemStyle: {
			  color: '#56AAC7'
			}
		  },
		  axisTick: {
			length: 12,
			lineStyle: {
			  color: '#fff',
			  width: 2
			}
		  },
		  splitLine: {

			length: 30,
			lineStyle: {
			  color: '#fff',
			  width: 4
			}
		  },
		  axisLabel: {
			color: '#293556',

			fontSize: 20,
			distance: -60,
			formatter: function (value) {
			  if (value === 10) {
				return 'L';
			  } else if (value === 50) {
				return 'M';
			  } else if (value === 90) {
				return 'H';
			  } 
			  return '';
			}
		  },

		  detail: {
			fontSize: 50,
			offsetCenter: [0, '0%'],
			valueAnimation: true,
			valueAnimation: true,
			color: '#66BB6A',
			formatter: '{value}',

		  },
		  data: [
			{
			  value: 70
			}
		  ]
		}
	  ]
	};
	// use configuration item and data specified to show chart

	//option && myChart.setOption(option);
	myChart.setOption(option);
	$(".MainColCls").niceScroll({
	  scrollspeed: 10,
	  cursorwidth: 5,
	  cursorborder: 0,
	  cursorcolor: '#0C090A',
	  cursorborderradius: 0,
	  autohidemode: true,
	  horizrailenabled: false
	});
  }
  var Onback = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	window.location = location.origin + "/web/wns_mnt/approve-loan"
  }
  var OnIdentityProof = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	var identity=_scope.submission.data["IdentityDownloadFile"];
	window.open(location.origin+"/image-services/doc/"+identity,'_blank')
  }
  var OnAddressProof = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	var address=_scope.submission.data["DownloadAddressFileName"];
	window.open(location.origin+"/image-services/doc/"+address,'_blank')
  }
  var OnApproved = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	var UpdateQuery = "UPDATE LoanDetail SET Status= 'Approved' WHERE LoanID ='"+_scope.submission.data["LoanID"]+"'";
	UpdateQuery = btoa(UpdateQuery);
	var res = AmperAxp.DataValidator.update(UpdateQuery, "DefaultDB");
  }
  var OnDeclined = function(){
	debugger;
	var _scope = this;
	var util = arguments[0];
	var UpdateQuery = "UPDATE LoanDetail SET Status= 'Declined' WHERE LoanID ='"+_scope.submission.data["LoanID"]+"'";
	UpdateQuery = btoa(UpdateQuery);
	var res = AmperAxp.DataValidator.update(UpdateQuery, "DefaultDB");
  }

  return { 
	deltakBarEchart:deltakBarEchart,
	Onback:Onback,
	OnIdentityProof:OnIdentityProof,
	OnAddressProof:OnAddressProof,
	OnApproved:OnApproved,
	OnDeclined:OnDeclined
  }

}());